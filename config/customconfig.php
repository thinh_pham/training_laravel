<?php

return [
    'url' => [
        'prefix_admin' => [
            'admin' => 'admin',
            'dashboard' => 'dashboard',
            'slider' => 'slider',
            'category' => 'category',
            'article' => 'article',
            'user' => 'user',
        ],
        'news' => 'news',
    ],
    'format' => [
        'long_time' => 'H:m:s d/m/Y',
        'short_time' => 'd/m/Y',
    ],
    'template' => [
        'form' => [
            'label' => ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'],
            'label_edit' => ['class' => 'control-label col-md-4 col-sm-4 col-xs-12'],
            'control' => ['class' => 'form-control col-md-7 col-xs-12'],
            'control_edit' => ['class' => 'form-control col-md-8 col-xs-12'],
        ],
        'status' => [
            'all' => ['name' => 'Tất cả', 'class' => 'btn-primary'],
            'active' => ['name' => 'Kích hoạt', 'class' => 'btn-success'],
            'inactive' => ['name' => 'Chưa kích hoạt', 'class' => 'btn-danger'],
            'default' => ['name' => 'Chưa xác định', 'class' => 'btn-default'],
        ],
        'visible' => [
            '1' => ['name' => 'Hiển thị', 'class' => 'btn-success'],
            '0' => ['name' => 'Ẩn', 'class' => 'btn-danger'],
            'default' => ['name' => 'Chưa xác định', 'class' => 'btn-default'],
        ],
        'display' => [
            'grid' => 'Dạng lưới',
            'list' => 'Dạng danh sách',
        ],
        'type' => [
            'feature' => 'Nổi bật',
            'normal' => 'Bình thường',
        ],
        'level' => [
            'admin' => 'Quản trị viên',
            'member' => 'Thành viên',
        ],
        'options' => [
            'edit' => [
                'title' => 'Chỉnh sửa',
                'class' => 'btn-success',
                'icon' => 'fa-pencil',
                'route-name' => '-form',
            ],
            'delete' => [
                'title' => 'Hủy bỏ',
                'class' => 'btn-danger btn-delete',
                'icon' => 'fa-trash',
                'route-name' => '-delete',
            ],
            'info' => [
                'title' => 'Chi tiết',
                'class' => 'btn-info',
                'icon' => 'fa-eye',
                'route-name' => '-form',
            ],
        ],
        'search' => [
            'all' => ['name' => 'Tìm kiếm tất cả'],
            'id' => ['name' => 'Tìm kiếm theo mã'],
            'name' => ['name' => 'Tìm kiếm theo tên'],
            'username' => ['name' => 'Tìm kiếm theo tên'],
            'fullname' => ['name' => 'Tìm kiếm theo tên đầy đủ'],
            'email' => ['name' => 'Tìm kiếm theo email'],
            'description' => ['name' => 'Tìm kiếm theo mô tả'],
            'link' => ['name' => 'Tìm kiếm theo liên kết'],
            'content' => ['name' => 'Tìm kiếm theo nội dung'],
        ],
    ],
    'area' => [
        'search' => [
            'default' => ['all', 'id', 'fullname'],
            'slider' => ['all', 'name', 'link'],
            'category' => ['all', 'name', 'link'],
            'article' => ['all', 'name', 'content'],
            'user' => ['all', 'username', 'fullname', 'email'],
        ],
        'options' => [
            'default' => ['edit', 'delete'],
            'slider' => ['edit', 'delete'],
            'category' => ['edit', 'delete'],
            'article' => ['edit', 'delete'],
            'user' => ['edit'],
        ],
    ],
];

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CategoryModel extends Model
{
    protected $table = 'category';
    private $uploadFolder = 'category';
    // protected $primaryKey = 'id';
    public $timestamps = true;
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    private $fieldAccepted = [
        'name',
        'created_by',
        'modified_by',
    ];
    private $crudNotAccepted = [
        '_token',
    ];

    public function listItem($params = null, $options = null)
    {
        $result = null;

        if ($options['task'] == 'back-end-list-item') {
            $query = self::select('id', 'name', 'status', 'created', 'created_by', 'modified', 'modified_by', 'is_home', 'display');

            if (isset($params['filter']['status']) && $params['filter']['status'] != 'all') {
                $query->where('status', '=', $params['filter']['status']);
            }

            if ($params['search']['value'] !== '') {
                if ($params['search']['field'] == 'all') {
                    $query = $query->where(function ($query) use ($params) {
                        foreach ($this->fieldAccepted as $field) {
                            $query->orWhere($field, 'like', "%{$params['search']['value']}%");
                        }
                    });
                } else if (in_array($params['search']['field'], $this->fieldAccepted)) {
                    $query->where($params['search']['field'], 'like', "%{$params['search']['value']}%");
                }
            }

            $result = $query->orderBy('id', 'desc')->paginate($params['pagination']['totalItemPerPage']);
        }
        if ($options['task'] == 'back-end-list-item-in-selectbox') {
            $query = self::select('id', 'name')->orderBy('name', 'asc')->where('status', 'active');

            $result = $query->pluck('name', 'id')->toArray();
        }
        if ($options['task'] == 'news-list-item') {
            $query = self::select('id', 'name')
                ->where('status', '=', 'active');
            $result = $query->get()->toArray();
        }
        if ($options['task'] == 'news-list-item-is-home') {
            $query = self::select('id', 'name', 'display')
                ->where('status', '=', 'active')
                ->where('is_home', '=', 1);
            $result = $query->get()->toArray();
        }

        return $result;
    }

    public function countItems($params = null, $options = null)
    {
        $result = null;

        if ($options['task'] == 'back-end-count-item') {
            $result = self::select('status', DB::raw('count(id) as count'))
                ->groupBy('status')
                ->get()->toArray();
        }

        return $result;
    }

    public function getItem($params = null, $options = null)
    {
        $result = null;
        if ($options['task'] == 'get-item') {
            $result = self::select('id', 'name', 'status', 'is_home', 'display')
                ->where('id', $params['item']['id'])->first();
        }
        if ($options['task'] == 'news-get-item') {
            $result = self::select('id', 'name', 'display')
                ->where('id', $params['category_id'])->first()->toArray();
        }
        return $result;
    }

    public function saveItem($params = null, $options = null)
    {
        if ($options['task'] == 'change-status') {
            $status = $params['item']['status'] == 'active' ? 'inactive' : 'active';
            self::where('id', $params['item']['id'])->update(['status' => $status]);
        }
        if ($options['task'] == 'change-visible') {
            $isHome = $params['item']['is_home'] == 1 ? 0 : 1;
            self::where('id', $params['item']['id'])->update(['is_home' => $isHome]);
        }
        if ($options['task'] == 'change-display') {
            self::where('id', $params['item']['id'])->update(['display' => $params['item']['display']]);
        }
        if ($options['task'] == 'back-end-add-item') {
            // created, modified
            $params['item']['created_by'] = 'thinhphamk8';
            $params['item']['created'] = date('Y-m-d H:i:s');
            $params['item']['modified_by'] = 'thinhphamk8';
            $params['item']['modified'] = date('Y-m-d H:i:s');
            $params['item'] = array_diff_key($params['item'], array_flip($this->crudNotAccepted));
            // save
            self::insert($params['item']);
        }
        if ($options['task'] == 'back-end-edit-item') {
            // created, modified
            $params['item']['created_by'] = 'thinhphamk8';
            $params['item']['created'] = date('Y-m-d H:i:s');
            $params['item']['modified_by'] = 'thinhphamk8';
            $params['item']['modified'] = date('Y-m-d H:i:s');
            $params['item'] = array_diff_key($params['item'], array_flip($this->crudNotAccepted));
            // save
            self::where('id', $params['item']['id'])->update($params['item']);

        }
    }

    public function deleteItem($params = null, $options = null)
    {
        if ($options['task'] == 'back-end-delete-item') {
            $item = self::getItem($params, ['task' => 'get-thumb']);
            Storage::disk('folder_upload_image')->delete($this->uploadFolder . '/' . $item['thumb']);
            self::where('id', $params['item']['id'])->delete();
        }
    }
}

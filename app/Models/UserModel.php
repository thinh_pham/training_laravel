<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UserModel extends Model
{
    protected $table = 'user';
    private $uploadFolder = 'user';
    // protected $primaryKey = 'id';
    public $timestamps = true;
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    private $fieldAccepted = [
        'id',
        'username',
        'email',
        'fullname',
    ];
    private $crudNotAccepted = [
        '_token',
        'current-avatar',
        'password_confirmation',
        'task',
    ];

    public function listItem($params = null, $options = null)
    {
        $result = null;

        if ($options['task'] == 'back-end-list-item') {
            $query = self::select('id', 'username', 'email', 'fullname', 'password', 'avatar', 'level', 'status', 'created', 'created_by', 'modified', 'modified_by');

            if (isset($params['filter']['status']) && $params['filter']['status'] != 'all') {
                $query->where('status', '=', $params['filter']['status']);
            }

            if ($params['search']['value'] !== '') {
                if ($params['search']['field'] == 'all') {
                    $query = $query->where(function ($query) use ($params) {
                        foreach ($this->fieldAccepted as $field) {
                            $query->orWhere($field, 'like', "%{$params['search']['value']}%");
                        }
                    });
                } else if (in_array($params['search']['field'], $this->fieldAccepted)) {
                    $query->where($params['search']['field'], 'like', "%{$params['search']['value']}%");
                }
            }

            $result = $query->orderBy('id', 'desc')->paginate($params['pagination']['totalItemPerPage']);
        }

        return $result;
    }

    public function countItems($params = null, $options = null)
    {
        $result = null;

        if ($options['task'] == 'back-end-count-item') {
            $result = self::select('status', DB::raw('count(id) as count'))
                ->groupBy('status')
                ->get()->toArray();
        }

        return $result;
    }

    public function getItem($params = null, $options = null)
    {
        $result = null;

        if ($options['task'] == 'get-item') {
            $result = self::select('id', 'username', 'email', 'fullname', 'password', 'avatar', 'level', 'status')
                ->where('id', $params['item']['id'])->first();
        }
        if ($options['task'] == 'get-avatar') {
            $result = self::select('id', 'avatar')
                ->where('id', $params['item']['id'])->first();
        }
        if ($options['task'] == 'auth-login') {
            $result = self::select('id', 'username', 'email', 'fullname', 'avatar', 'level')
                ->where('status', 'active')
                ->where('email', $params['email'])
                ->where('password', md5($params['password']))->first();

            $result = $result ? $result->toArray() : $result;

        }

        return $result;
    }

    public function saveItem($params = null, $options = null)
    {
        if ($options['task'] == 'change-status') {
            $status = $params['item']['status'] == 'active' ? 'inactive' : 'active';
            self::where('id', $params['item']['id'])->update(['status' => $status]);
        }
        if ($options['task'] == 'change-level') {
            self::where('id', $params['item']['id'])->update(['level' => $params['item']['level']]);
        }
        if ($options['task'] == 'change-password') {
            $params['item']['password'] = md5($params['item']['password']);
            self::where('id', $params['item']['id'])->update(['password' => $params['item']['password']]);
        }
        if ($options['task'] == 'back-end-add-item') {
            // upload images
            $avatar = $params['item']['avatar'];
            $params['item']['avatar'] = Str::random(10) . '.' . $avatar->clientExtension();
            $avatar->storeAs($this->uploadFolder, $params['item']['avatar'], 'folder_upload_image');
            // encrypt password
            $params['item']['password'] = md5($params['item']['password']);
            // created, modified
            $params['item']['created_by'] = 'thinhphamk8';
            $params['item']['created'] = date('Y-m-d H:i:s');
            $params['item']['modified_by'] = 'thinhphamk8';
            $params['item']['modified'] = date('Y-m-d H:i:s');
            $params['item'] = array_diff_key($params['item'], array_flip($this->crudNotAccepted));
            // save
            self::insert($params['item']);
        }
        if ($options['task'] == 'back-end-edit-item') {
            // delete images
            $item = self::getItem($params, ['task' => 'get-avatar']);
            Storage::disk('folder_upload_image')->delete($this->uploadFolder . '/' . $item['avatar']);
            // upload images
            $avatar = $params['item']['avatar'];
            $params['item']['avatar'] = Str::random(10) . '.' . $avatar->clientExtension();
            $avatar->storeAs($this->uploadFolder, $params['item']['avatar'], 'folder_upload_image');
            // created, modified
            $params['item']['created_by'] = 'thinhphamk8';
            $params['item']['created'] = date('Y-m-d H:i:s');
            $params['item']['modified_by'] = 'thinhphamk8';
            $params['item']['modified'] = date('Y-m-d H:i:s');
            $params['item'] = array_diff_key($params['item'], array_flip($this->crudNotAccepted));
            // save
            self::where('id', $params['item']['id'])->update($params['item']);
        }
    }

    public function deleteItem($params = null, $options = null)
    {
        if ($options['task'] == 'back-end-delete-item') {
            $item = self::getItem($params, ['task' => 'get-avatar']);
            Storage::disk('folder_upload_image')->delete($this->uploadFolder . '/' . $item['avatar']);
            self::where('id', $params['item']['id'])->delete();
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class SliderModel extends Model
{
    protected $table = 'slider';
    private $uploadFolder = 'slider';
    // protected $primaryKey = 'id';
    public $timestamps = true;
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    private $fieldAccepted = [
        'id',
        'name',
        'link',
        'created_by',
        'modified_by',
    ];
    private $crudNotAccepted = [
        '_token',
        'current-thumb',
    ];

    public function listItem($params = null, $options = null)
    {
        $result = null;

        if ($options['task'] == 'back-end-list-item') {
            $query = self::select('id', 'name', 'description', 'link', 'thumb', 'created', 'created_by', 'modified', 'modified_by', 'status');

            if (isset($params['filter']['status']) && $params['filter']['status'] != 'all') {
                $query->where('status', '=', $params['filter']['status']);
            }

            if ($params['search']['value'] !== '') {
                if ($params['search']['field'] == 'all') {
                    $query = $query->where(function ($query) use ($params) {
                        foreach ($this->fieldAccepted as $field) {
                            $query->orWhere($field, 'like', "%{$params['search']['value']}%");
                        }
                    });
                } else if (in_array($params['search']['field'], $this->fieldAccepted)) {
                    $query->where($params['search']['field'], 'like', "%{$params['search']['value']}%");
                }
            }

            $result = $query->orderBy('id', 'desc')->paginate($params['pagination']['totalItemPerPage']);
        }
        if ($options['task'] == 'news-list-item') {
            $query = self::select('id', 'name', 'description', 'link', 'thumb', 'status')->where('status', '=', 'active');
            $result = $query->get()->toArray();
        }

        return $result;
    }

    public function countItems($params = null, $options = null)
    {
        $result = null;

        if ($options['task'] == 'back-end-count-item') {
            $result = self::select('status', DB::raw('count(id) as count'))
                ->groupBy('status')
                ->get()->toArray();
        }

        return $result;
    }

    public function getItem($params = null, $options = null)
    {
        $result = null;
        if ($options['task'] == 'get-item') {
            $result = self::select('id', 'name', 'description', 'link', 'thumb', 'status')
                ->where('id', $params['item']['id'])->first();
        }
        if ($options['task'] == 'get-thumb') {
            $result = self::select('id', 'thumb')
                ->where('id', $params['item']['id'])->first();
        }
        return $result;
    }

    public function saveItem($params = null, $options = null)
    {
        if ($options['task'] == 'change-status') {
            $status = $params['item']['status'] == 'active' ? 'inactive' : 'active';
            self::where('id', $params['item']['id'])->update(['status' => $status]);
        }
        if ($options['task'] == 'back-end-add-item') {
            // upload images
            $thumb = $params['item']['thumb'];
            $params['item']['thumb'] = Str::random(10) . '.' . $thumb->clientExtension();
            $thumb->storeAs($this->uploadFolder, $params['item']['thumb'], 'folder_upload_image');
            // created, modified
            $params['item']['created_by'] = 'thinhphamk8';
            $params['item']['created'] = date('Y-m-d H:i:s');
            $params['item']['modified_by'] = 'thinhphamk8';
            $params['item']['modified'] = date('Y-m-d H:i:s');
            $params['item'] = array_diff_key($params['item'], array_flip($this->crudNotAccepted));
            // save
            self::insert($params['item']);
        }
        if ($options['task'] == 'back-end-edit-item') {
            // delete images
            $item = self::getItem($params, ['task' => 'get-thumb']);
            Storage::disk('folder_upload_image')->delete($this->uploadFolder . '/' . $item['thumb']);
            // upload images
            $thumb = $params['item']['thumb'];
            $params['item']['thumb'] = Str::random(10) . '.' . $thumb->clientExtension();
            $thumb->storeAs($this->uploadFolder, $params['item']['thumb'], 'folder_upload_image');
            // created, modified
            $params['item']['created_by'] = 'thinhphamk8';
            $params['item']['created'] = date('Y-m-d H:i:s');
            $params['item']['modified_by'] = 'thinhphamk8';
            $params['item']['modified'] = date('Y-m-d H:i:s');
            $params['item'] = array_diff_key($params['item'], array_flip($this->crudNotAccepted));
            // save
            self::where('id', $params['item']['id'])->update($params['item']);

        }
    }

    public function deleteItem($params = null, $options = null)
    {
        if ($options['task'] == 'back-end-delete-item') {
            $item = self::getItem($params, ['task' => 'get-thumb']);
            Storage::disk('folder_upload_image')->delete($this->uploadFolder . '/' . $item['thumb']);
            self::where('id', $params['item']['id'])->delete();
        }
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $condition = [];
        switch ($this->task) {
            case 'add':
                $condition = [
                    'username' => 'bail|required|between:5,30|unique:App\Models\UserModel,username,',
                    'email' => 'bail|required|min:5|email|unique:App\Models\UserModel,email,',
                    'fullname' => 'bail|required|min:5',
                    'password' => 'bail|required|min:5|confirmed',
                    'status' => 'bail|in:active,inactive',
                    'level' => 'bail|in:admin,member',
                    'avatar' => 'bail|required|image|max:500',
                ];
                break;
            case 'edit':
                $condition = [
                    'username' => 'bail|required|between:5,30|unique:App\Models\UserModel,username,' . $this->id,
                    'email' => 'bail|required|min:5|email|unique:App\Models\UserModel,email,' . $this->id,
                    'fullname' => 'bail|required|min:5',
                    'status' => 'bail|in:active,inactive',
                    'avatar' => 'bail|required|image|max:500',
                ];
                break;
            case 'change-password':
                $condition = [
                    'password' => 'bail|required|min:5|confirmed',
                ];
                break;
            case 'change-level':
                $condition = [
                    'level' => 'bail|in:admin,member',
                ];
                break;
        }

        return $condition;
    }

    public function messages()
    {
        return [];
    }
}

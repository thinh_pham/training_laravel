<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'content' => 'required|min:5',
            'thumb' => 'bail|required|image',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '<strong>- Tên:</strong> là trường bắt buộc',
            'content.required' => '<strong>- Nội dung:</strong> là trường bắt buộc',
            'thumb.required' => '<strong>- Hình ảnh:</strong> là trường bắt buộc',
            'thumb.image' => '<strong>- Hình ảnh:</strong> không đúng định dạng',
        ];
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class DashboardController extends Controller
{
    private $pathView = 'back-end.pages.dashboard.';
    private $controllerName = 'dashboard';

    public function __construct()
    {
        // View::share('controllerName', $this->controllerName);
        // or
        view()->share('controllerName', $this->controllerName);
    }

    public function index()
    {
        return view($this->pathView . 'index');
    }
}

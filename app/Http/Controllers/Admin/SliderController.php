<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SliderRequest as MainRequest;
use App\Models\SliderModel as MainModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class SliderController extends Controller
{
    private $pathView = 'back-end.pages.slider.';
    private $controllerName = 'slider';
    private $params = [];
    private $model;

    public function __construct()
    {
        $this->params['pagination']['totalItemPerPage'] = 5;
        $this->model = new MainModel();
        view()->share('controllerName', $this->controllerName);
        // or
        // View::share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {
        // params
        $this->params['filter']['status'] = $request->input('filter_status', 'all');
        $this->params['search']['field'] = $request->input('search_field', 'all');
        $this->params['search']['value'] = $request->input('search_value', '');

        $items = $this->model->listItem($this->params, ['task' => 'back-end-list-item']);
        $itemsByStatus = $this->model->countItems($this->params, ['task' => 'back-end-count-item']);

        return view($this->pathView . 'index', [
            'items' => $items,
            'itemsByStatus' => $itemsByStatus,
            'params' => $this->params,
        ]);
    }

    public function form(Request $request)
    {
        $item = null;
        if ($request->id !== '') {
            $this->params['item']['id'] = $request->id;
            $item = $this->model->getItem($this->params, ['task' => 'get-item']);
        }
        return view($this->pathView . 'form', ['item' => $item]);
    }

    public function save(MainRequest $request)
    {
        if ($request->method() == 'POST') {
            $this->params['item'] = $request->all();

            $task = 'back-end-add-item';
            $notify = 'Thêm phần tử mới thành công';
            if ($this->params['item']['id'] !== null) {
                $task = 'back-end-edit-item';
                $notify = 'Cập nhật phần tử thành công';
            }
            $this->model->saveItem($this->params, ['task' => $task]);
            return redirect()->route($this->controllerName)->with('notify', $notify);
        }
    }

    public function status(Request $request)
    {
        $this->params['item']['status'] = $request->status;
        $this->params['item']['id'] = $request->id;
        $this->model->saveItem($this->params, ['task' => 'change-status']);
        return redirect()->route($this->controllerName);
    }

    public function delete(Request $request)
    {
        $this->params['item']['id'] = $request->id;
        $this->model->deleteItem($this->params, ['task' => 'back-end-delete-item']);
        return redirect()->route($this->controllerName, )->with('notify', 'Successfully');
    }
}

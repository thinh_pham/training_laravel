<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest as MainRequest;
use App\Models\CategoryModel as CategoryModel;
use App\Models\UserModel as MainModel;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $pathView = 'back-end.pages.user.';
    private $controllerName = 'user';
    private $params = [];
    private $model;

    public function __construct()
    {
        $this->params['pagination']['totalItemPerPage'] = 5;
        $this->model = new MainModel();
        view()->share('controllerName', $this->controllerName);
        // or
        // View::share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {
        // params
        $this->params['filter']['status'] = $request->input('filter_status', 'all');
        $this->params['search']['field'] = $request->input('search_field', 'all');
        $this->params['search']['value'] = $request->input('search_value', '');

        $items = $this->model->listItem($this->params, ['task' => 'back-end-list-item']);
        $itemsByStatus = $this->model->countItems($this->params, ['task' => 'back-end-count-item']);

        return view($this->pathView . 'index', [
            'items' => $items,
            'itemsByStatus' => $itemsByStatus,
            'params' => $this->params,
        ]);
    }

    public function form(Request $request)
    {
        $categoryModel = new CategoryModel();
        $itemsCategory = $categoryModel->listItem(null, ['task' => 'back-end-list-item-in-selectbox']);
        $item = null;
        if ($request->id !== '') {
            $this->params['item']['id'] = $request->id;
            $item = $this->model->getItem($this->params, ['task' => 'get-item']);
        }
        return view($this->pathView . 'form', [
            'item' => $item,
            'itemsCategory' => $itemsCategory,
        ]);
    }

    public function save(MainRequest $request)
    {
        if ($request->method() == 'POST') {
            $this->params['item'] = $request->all();
            $task = 'back-end-add-item';
            $notify = 'Thêm phần tử mới thành công';
            if ($this->params['item']['id'] !== null) {
                $task = 'back-end-edit-item';
                $notify = 'Cập nhật phần tử thành công';
            }
            $this->model->saveItem($this->params, ['task' => $task]);
            return redirect()->route($this->controllerName)->with('notify', $notify);
        }
    }

    public function changePassword(MainRequest $request)
    {
        if ($request->method() == 'POST') {
            $this->params['item'] = $request->all();
            $this->model->saveItem($this->params, ['task' => 'change-password']);
            return redirect()->route($this->controllerName)->with('notify', 'Cập nhật mật khẩu thành công');
        }
    }

    public function changeLevel(MainRequest $request)
    {
        if ($request->method() == 'POST') {
            $this->params['item'] = $request->all();
            $this->model->saveItem($this->params, ['task' => 'change-level']);
            return redirect()->route($this->controllerName)->with('notify', 'Cập nhật mật khẩu thành công');
        }
    }

    public function status(Request $request)
    {
        $this->params['item']['status'] = $request->status;
        $this->params['item']['id'] = $request->id;
        $this->model->saveItem($this->params, ['task' => 'change-status']);
        return redirect()->route($this->controllerName)->with('notify', 'Thay đổi đã được cập nhật');
    }

    public function level(Request $request)
    {
        $this->params['item']['level'] = $request->level;
        $this->params['item']['id'] = $request->id;
        $this->model->saveItem($this->params, ['task' => 'change-level']);
        return redirect()->route($this->controllerName)->with('notify', 'Thay đổi đã được cập nhật');
    }

    public function delete(Request $request)
    {
        $this->params['item']['id'] = $request->id;
        $this->model->deleteItem($this->params, ['task' => 'back-end-delete-item']);
        return redirect()->route($this->controllerName, )->with('notify', 'Xóa phần tử thành công');
    }
}

<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Models\ArticleModel as ArticleModel;
use App\Models\CategoryModel as CategoryModel;
use App\Models\SliderModel as SliderModel;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $pathView = 'news.pages.category.';
    private $controllerName = 'category';
    private $params = [];

    public function __construct()
    {
        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {
        $this->params['category_id'] = $request->category_id;
        $sliderModel = new SliderModel();
        $categoryModel = new CategoryModel();
        $articleModel = new ArticleModel();

        $itemCategory = $categoryModel->getItem($this->params, ['task' => 'news-get-item']);
        $itemCategory['article'] = $articleModel->listItem($this->params, ['task' => 'news-list-item-in-category']);
        $itemsLatest = $articleModel->listItem(null, ['task' => 'news-list-item-latest']);

        return view($this->pathView . 'index', [
            'params' => $this->params,
            'itemsLatest' => $itemsLatest,
            'itemCategory' => $itemCategory,
        ]);
    }
}

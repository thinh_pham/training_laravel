<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Models\ArticleModel as ArticleModel;

class NotifyController extends Controller
{
    private $pathView = 'news.pages.notify.';
    private $controllerName = 'notify';
    private $params = [];
    private $model;

    public function __construct()
    {
        view()->share('controllerName', $this->controllerName);
    }

    public function noPermission()
    {
        $articleModel = new ArticleModel();
        $itemsLatest = $articleModel->listItem(null, ['task' => 'news-list-item-latest']);

        return view($this->pathView . 'no-permission', [
            'itemsLatest' => $itemsLatest,
        ]);
    }
}

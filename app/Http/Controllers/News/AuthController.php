<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest as MainRequest;
use App\Models\UserModel;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $pathView = 'news.pages.auth.';
    private $controllerName = 'auth';
    private $params = [];
    private $model;

    public function __construct()
    {
        view()->share('controllerName', $this->controllerName);
    }

    public function login(Request $request)
    {
        return view($this->pathView . 'login');
    }

    public function postLogin(MainRequest $request)
    {
        if ($request->method() == 'POST') {
            $this->params = $request->all();

            $userModel = new UserModel();
            $userInfo = $userModel->getItem($this->params, ['task' => 'auth-login']);

            if (!$userInfo) {
                return redirect()->route($this->controllerName . '-login')->with('notify', 'Tài khoản hoặc mật khẩu sai');
            }

            $request->session()->put('userInfo', $userInfo);
            return redirect()->route('home');
        }
    }

    public function logout(Request $request)
    {
        if ($request->session()->has('userInfo')) {
            $request->session()->pull('userInfo');
        }

        return redirect()->route('home');
    }
}

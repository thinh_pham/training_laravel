<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Models\ArticleModel as ArticleModel;
use App\Models\CategoryModel as CategoryModel;
use App\Models\SliderModel as SliderModel;

class HomeController extends Controller
{
    private $pathView = 'news.pages.home.';
    private $controllerName = 'home';
    private $params = [];

    public function __construct()
    {
        view()->share('controllerName', $this->controllerName);
    }

    public function index()
    {
        $sliderModel = new SliderModel();
        $categoryModel = new CategoryModel();
        $articleModel = new ArticleModel();

        $itemsSlider = $sliderModel->listItem(null, ['task' => 'news-list-item']);
        $itemsCategory = $categoryModel->listItem(null, ['task' => 'news-list-item-is-home']);
        $itemsFeatured = $articleModel->listItem(null, ['task' => 'news-list-item-featured']);
        $itemsLatest = $articleModel->listItem(null, ['task' => 'news-list-item-latest']);

        foreach ($itemsCategory as $key => $category) {
            $itemsCategory[$key]['article'] = $articleModel->listItem(['category_id' => $category['id']], ['task' => 'news-list-item-in-category']);
        }

        return view($this->pathView . 'index', [
            'itemsSlider' => $itemsSlider,
            'itemsCategory' => $itemsCategory,
            'itemsFeatured' => $itemsFeatured,
            'itemsLatest' => $itemsLatest,
        ]);
    }
}

<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Models\ArticleModel as ArticleModel;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    private $pathView = 'news.pages.article.';
    private $controllerName = 'article';
    private $params = [];

    public function __construct()
    {
        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {
        $this->params['article_id'] = $request->article_id;
        $articleModel = new ArticleModel();

        $itemArticle = $articleModel->getItem($this->params, ['task' => 'news-get-item']);
        $itemsLatest = $articleModel->listItem(null, ['task' => 'news-list-item-latest']);
        $this->params['category_id'] = $itemArticle['category_id'];
        $itemArticle['related'] = $articleModel->listItem($this->params, ['task' => 'news-list-item-related']);

        return view($this->pathView . 'index', [
            'params' => $this->params,
            'itemArticle' => $itemArticle,
            'itemsLatest' => $itemsLatest,
        ]);
    }
}

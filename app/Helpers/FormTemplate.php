<?php

namespace App\Helpers;

class FormTemplate
{
    public static function show($elements)
    {
        $xhtml = null;
        if (count($elements) > 0) {
            foreach ($elements as $element) {
                $xhtml .= self::formGroup($element);
            }
        }
        return $xhtml;
    }

    public static function formGroup($element)
    {
        $type = isset($element['type']) ? $element['type'] : 'normal';
        $xhtml = null;
        switch ($type) {
            case 'normal':
                $xhtml .= sprintf(
                    '<div class="form-group">%s<div class="col-md-6 col-sm-6 col-xs-12">%s</div></div>',
                    $element['label'],
                    $element['input'],
                );
                break;
            case 'option':
                $xhtml .= sprintf(
                    '<div class="form-group">%s<div class="col-md-6 col-sm-6 col-xs-12">%s</div></div>',
                    $element['label'],
                    self::radioGroup($element['input']),
                );
                break;
            case 'file':
                $xhtml .= sprintf(
                    '<div class="form-group">%s<div class="col-md-6 col-sm-6 col-xs-12">%s</div></div>
                    <div class="form-group"><div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">%s</div></div>',
                    $element['label'],
                    $element['input'],
                    isset($element['thumb']) ? $element['thumb'] : $element['avatar']
                );
                break;
            case 'btn':
                $xhtml .= sprintf(
                    '<div class="ln_solid"></div>
                    <div class="form-group"><div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">%s</div></div>',
                    $element['input'],
                );
                break;
        }
        return $xhtml;
    }

    public static function radioGroup($options)
    {
        $xhtml = null;
        if (count($options) > 0) {
            foreach ($options as $option) {
                $xhtml .= '<div class="radio radio-inline">';
                $xhtml .= sprintf(
                    '<label>%s<i class="helper"></i>%s</label>',
                    $option['control'],
                    $option['name']
                );
                $xhtml .= '</div>';
            }
        }
        return $xhtml;
    }
}

<?php

namespace App\Helpers;

use App\Helpers\Highlight as Highlight;

class Template
{
    public static function showItemImage($controller, $thumb, $alt)
    {
        $xhtml = sprintf(
            '<img src="%s" alt="%s" class="zvn-thumb" />',
            asset('images/' . $controller . '/' . $thumb),
            $alt
        );

        return $xhtml;
    }

    public static function showItemStatus($controller, $id, $status)
    {
        $tmpStatus = config('customconfig.template.status');

        $status = array_key_exists($status, $tmpStatus) ? $status : 'default';
        // $curStatus = $tmpStatus[$status];
        $curStatus = $tmpStatus[$status];
        $linkStatus = route($controller . '-status', ['status' => $status, 'id' => $id]);

        $xhtml = sprintf(
            '<a href="%s" type="button" class="btn btn-round %s">%s</a>',
            $linkStatus,
            $curStatus['class'],
            $curStatus['name']
        );

        return $xhtml;
    }

    public static function showItemVisible($controller, $id, $isHome)
    {
        $tmpVisible = config('customconfig.template.visible');

        $isHome = array_key_exists($isHome, $tmpVisible) ? $isHome : 'default';
        // $curStatus = $tmpStatus[$status];
        $curVisible = $tmpVisible[$isHome];
        $linkVisible = route($controller . '-visible', ['visible' => $isHome, 'id' => $id]);

        $xhtml = sprintf(
            '<a href="%s" type="button" class="btn btn-round %s">%s</a>',
            $linkVisible,
            $curVisible['class'],
            $curVisible['name']
        );

        return $xhtml;
    }

    public static function showItemSelect($controller, $id, $value, $field)
    {
        $tmpField = config('customconfig.template.' . $field);

        $curvalue = array_key_exists($value, $tmpField) ? $value : 'default';
        $curField = $tmpField[$value];
        $linkField = route($controller . '-' . $field, [$field => 'value_new', 'id' => $id]);

        $xhtml = sprintf(
            '<select name="display-selected" data-url="%s"class="form-control">',
            $linkField
        );
        foreach ($tmpField as $key => $value) {
            $selected = $key === $curvalue ? 'selected' : '';
            $xhtml .= sprintf(
                '<option value="%s" %s>%s</option>',
                $key,
                $selected,
                $value
            );
        }

        return $xhtml . '</select>';
    }

    public static function showItemHistory($by, $time, $params)
    {
        $xhtml = sprintf(
            '<p><i class="fa fa-user"> %s</i></p><p><i class="fa fa-clock-o"></i> %s</p>',
            Highlight::show($by, 'all', $params),
            date(config('customconfig.format.long_time', 'H:m:s d/m/Y'), strtotime($time))
        );

        return $xhtml;
    }

    public static function showItemOptions($controller, $id)
    {
        $tmpButton = config('customconfig.template.options');

        $buttonInArea = config('customconfig.area.options');

        $controller = array_key_exists($controller, $buttonInArea) ? $controller : 'default';
        $listButton = $buttonInArea[$controller];

        $xhtml = '<div class="zvn-box-btn-filter">';

        foreach ($listButton as $btn) {
            $curButton = $tmpButton[$btn];
            $xhtml .= sprintf(
                '<a href="%s" type="button" class="btn btn-icon %s"
                data-toggle="tooltip" data-placement="top" data-original-title="%s">
                <i class="fa %s"></i></a>',
                route($controller . $curButton['route-name'], ['id' => $id]),
                $curButton['class'],
                $curButton['title'],
                $curButton['icon']
            );
        }

        return $xhtml . '</div>';
    }

    public static function showFilterButton($controller, $itemsByStatus, $filterStatus)
    {
        // get config about search-area from config/customconfig.php
        $tmpStatus = config('customconfig.template.status');
        //
        $xhtml = null;
        // check $itemsByStatus array is empty
        if (count($itemsByStatus) > 0) {
            // put elemnent at beginning of the array
            array_unshift($itemsByStatus, [
                'status' => 'all',
                // count all elements in array
                'count' => array_sum(array_column($itemsByStatus, 'count')),
            ]);
            // browse array and display
            foreach ($itemsByStatus as $item) {
                /**
                 * check key exists in array
                 * if not exists default value: default
                 */
                $status = array_key_exists($item['status'], $tmpStatus) ? $item['status'] : 'default';
                // get current status
                $curStatus = $tmpStatus[$status];
                // check current selected status, then assign class
                $classActive = ($status == $filterStatus) ? 'btn-success' : 'btn-default';

                $xhtml .= sprintf(
                    '<a href="%s" type="button" class="btn %s btn-filter-status">
                        %s <span class="badge">%s</span>
                    </a>',
                    route($controller) . '?filter_status=' . $status,
                    $classActive,
                    ucfirst($curStatus['name']),
                    $item['count']
                );
            }
        }

        return $xhtml;
    }

    public static function showSearchArea($controller, $search)
    {
        // get config about search-area from config/customconfig.php
        $tmpSearchField = config('customconfig.template.search');
        $fieldInController = config('customconfig.area.search');
        /**
         * check key exists in array search-area
         * if not exists default value: default
         * $controller: slider, category, user, article or ...
         */
        $controller = array_key_exists($controller, $fieldInController) ? $controller : 'default';
        /**
         * check field value exists in array $fieldInController[$controller]
         * if not exists default value: all
         * $search['field']: all, id, name, description, email, or ...
         */
        $searchField = in_array($search['field'], $fieldInController[$controller]) ? $search['field'] : 'all';
        //
        $xhtml = null;
        $xhtmlField = null;
        // get list search-field link
        foreach ($fieldInController[$controller] as $field) {
            $xhtmlField .= sprintf(
                '<li><a href="javascript;" class="select-field" data-field="%s">%s</a></li>',
                $field,
                $tmpSearchField[$field]['name']
            );
        }
        //
        $xhtml .= sprintf(
            '<div class="input-group">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle btn-active-field"
                        data-toggle="dropdown" aria-expanded="false">
                        %s <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" role="menu">%s</ul>
                </div>
                <input type="text" class="form-control" name="search_value" value="%s">
                <span class="input-group-btn">
                    <button id="btn-clear" type="button" class="btn btn-success"
                        style="margin-right: 0px">Xóa tìm
                        kiếm</button>
                    <button id="btn-search" type="button" class="btn btn-primary">Tìm kiếm</button>
                </span>
                <input type="hidden" name="search_field" value="all">
            </div>',
            $tmpSearchField[$searchField]['name'],
            $xhtmlField,
            $search['value'] == '' ? '' : $search['value']
        );
        return $xhtml;
    }

    public static function showDateTime($time)
    {
        return date_format(date_create($time), config('customconfig.format.short_time'));
    }

    public static function showContent($content, $length, $prefix = '...')
    {
        $prefix = $length == 0 ? '' : $prefix;
        $content = str_replace(['<p>', '</p>'], '', $content);
        return preg_replace('/\s+?(\S+)$/', '', substr($content, 0, $length)) . $prefix;
    }
}

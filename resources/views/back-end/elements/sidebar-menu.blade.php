<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>Tổng quan</h3>
        <ul class="nav side-menu">
            <li class="{{$controllerName === 'dashboard' ? 'active' : ''}}"><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Trang chủ</a></li>
            <li class="{{$controllerName === 'user' ? 'active' : ''}}"><a href="{{ route('user') }}"><i class="fa fa-user"></i> Người dùng</a></li>
            <li class="{{$controllerName === 'category' ? 'active' : ''}}"><a href="{{ route('category') }}"><i class="fa fa fa-building-o"></i> Thể loại</a></li>
            <li class="{{$controllerName === 'article' ? 'active' : ''}}"><a href="{{ route('article') }}"><i class="fa fa-newspaper-o"></i> Bài viết</a></li>
            <li class="{{$controllerName === 'slider' ? 'active' : ''}}"><a href="{{ route('slider') }}"><i class="fa fa-sliders"></i> Thanh trượt</a></li>
        </ul>
    </div>
</div>

<div class="left_col scroll-view">
    @include('back-end.elements.sidebar-title')
    <!-- /menu profile quick info -->
    <br />
    <!-- sidebar menu -->
    @include('back-end.elements.sidebar-menu')
    <!-- /sidebar menu -->
    <!-- /menu footer buttons -->
    @include('back-end.elements.sidebar-bottom')
    <!-- /menu footer buttons -->
</div>

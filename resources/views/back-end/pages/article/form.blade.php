@php
use App\Helpers\Template as Template;
use App\Helpers\FormTemplate as FormTemplate;

$formLabelClass = config('customconfig.template.form.label.class');
$formControlClass = config('customconfig.template.form.control.class');
$controlHiddenID = Form::hidden('id', isset($item['id']) ? $item['id'] : '');
$controlHiddenThumb = Form::hidden('current-thumb', isset($item['thumb']) ? $item['thumb'] : '');
$statusTmp = config('customconfig.template.status');
$currenStatus = isset($item['status']) ? $item['status'] : '';

$elements = [
    [
        'label' => Form::label('name', 'Tên', ['class' => $formLabelClass]),
        'input' => Form::text(
            'name',
            $item['name'] ?? '',
            ['class' => $formControlClass]),
    ],
    [
        'label' => Form::label('content', 'Nội dung', ['class' => $formLabelClass]),
        'input' => Form::textarea(
            'content',
            $item['content'] ?? '',
            ['class' => $formControlClass . ' mw-100 ckeditor']),
    ],
    [
        'label' => Form::label('status', 'Trạng thái', ['class' => $formLabelClass]),
        'input' => [
            'active'=> [
                'name' => $statusTmp['active']['name'],
                'control' => Form::radio('status', 'active', true, ['id' => 'status-active']),
            ],
            'inactive'=> [
                'name' => $statusTmp['inactive']['name'],
                'control' => Form::radio('status', 'inactive', $currenStatus === 'inactive' ? true : false, ['id' => 'status-inactive']),
            ],
        ],
        'type' => 'option',
    ],
    [
        'label' => Form::label('category_id', 'Thể loại', ['class' => $formLabelClass]),
        'input' => Form::select(
            'category_id',
            $itemsCategory,
            $item['category_id'] ?? null,
            ['class' => $formControlClass]),
    ],
    [
        'label' => Form::label('thumb', 'Hình', ['class' => $formLabelClass]),
        'input' => Form::file('thumb', ['class' => $formControlClass]),
        'thumb' => isset($item['thumb']) ? Template::showItemImage($controllerName, $item['thumb'], $item['name']) : '',
        'type' => 'file',
    ],
    [
        'input' => $controlHiddenID . $controlHiddenThumb . Form::submit('Lưu', ['class' => 'btn btn-success', 'style' => 'width: 100%;']),
        'type' => 'btn',
    ],
];
@endphp

@extends('back-end.main')
@section('page-content')
    @include('back-end.templates.x_page_header', ['pageIndex' => false, 'pageName' => ''])
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                @include('back-end.templates.x_title', ['title' => 'Thêm mới'])
                <div class="x_content">
                    <br />
                    @include('back-end.templates.error')
                    {{-- form --}}
                    {{ Form::open([
                            'url' => route($controllerName . '-save'),
                            'method' => 'POST',
                            'accept-charset' => 'UTF-8',
                            'enctype' => 'multipart/form-data',
                            'id' => 'demo-form2',
                            'class' => 'form-horizontal form-label-left',
                            'data-parsley-validate',
                        ]) }}
                        {{-- form rows --}}
                        {!! FormTemplate::show($elements) !!}
                    {{ Form::close() }}
                    {{-- // --}}
                </div>
            </div>
        </div>
    </div>
@endsection

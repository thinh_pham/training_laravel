@php
use App\Helpers\Template as Template;
use App\Helpers\FormTemplate as FormTemplate;

$formLabelClass = config('customconfig.template.form.label.class');
$formControlClass = config('customconfig.template.form.control.class');
$controlHiddenID = Form::hidden('id', isset($item['id']) ? $item['id'] : '');
$controlHiddenThumb = Form::hidden('current-thumb', isset($item['thumb']) ? $item['thumb'] : '');
$statusTmp = config('customconfig.template.status');
$visibleTmp = config('customconfig.template.visible');
$displayTmp = config('customconfig.template.display');
$currentStatus = isset($item['status']) ? $item['status'] : '';
$currentVisible = isset($item['is_home']) ? $item['is_home'] : '';

$elements = [
    [
        'label' => Form::label('name', 'Tên', ['class' => $formLabelClass]),
        'input' => Form::text(
            'name',
            $item['name'] ?? '',
            ['class' => $formControlClass]),
    ],
    [
        'label' => Form::label('status', 'Trạng thái', ['class' => $formLabelClass]),
        'input' => [
            'active'=> [
                'name' => $statusTmp['active']['name'],
                'control' => Form::radio('status', 'active', true, ['id' => 'status-active']),
            ],
            'inactive'=> [
                'name' => $statusTmp['inactive']['name'],
                'control' => Form::radio('status', 'inactive', $currentStatus === 'inactive' ? true : false, ['id' => 'status-inactive']),
            ],
        ],
        'type' => 'option',
    ],
    [
        'label' => Form::label('is_home', 'Có thể thấy', ['class' => $formLabelClass]),
        'input' => [
            [
                'name' => $visibleTmp['1']['name'],
                'control' => Form::radio('is_home', 1, true, ['id' => 'visible-1']),
            ],
            [
                'name' => $visibleTmp['0']['name'],
                'control' => Form::radio('is_home', 0, $currentVisible === 0 ? true : false, ['id' => 'visible-0']),
            ],
        ],
        'type' => 'option',
    ],
    [
        'label' => Form::label('display', 'Hiển thị', ['class' => $formLabelClass]),
        'input' => Form::select(
            'display',
            $displayTmp,
            $item['display'] ?? null,
            ['class' => $formControlClass]),
    ],
    [
        'input' => $controlHiddenID . Form::submit('Lưu', ['class' => 'btn btn-success', 'style' => 'width: 100%;']),
        'type' => 'btn',
    ],
];
@endphp

@extends('back-end.main')
@section('page-content')
    @include('back-end.templates.x_page_header', ['pageIndex' => false])
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                @include('back-end.templates.x_title', ['title' => 'Thêm mới'])
                <div class="x_content">
                    <br />
                    @include('back-end.templates.error')
                    {{-- form --}}
                    {{ Form::open([
                            'url' => route($controllerName . '-save'),
                            'method' => 'POST',
                            'accept-charset' => 'UTF-8',
                            'enctype' => 'multipart/form-data',
                            'id' => 'demo-form2',
                            'class' => 'form-horizontal form-label-left',
                            'data-parsley-validate',
                        ]) }}
                        {{-- form rows --}}
                        {!! FormTemplate::show($elements) !!}

                    {{ Form::close() }}
                    {{-- // --}}
                </div>
            </div>
        </div>
    </div>
@endsection

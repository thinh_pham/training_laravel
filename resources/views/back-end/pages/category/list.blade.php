@php
use App\Helpers\Template as Template;
use App\Helpers\Highlight as Highlight;
@endphp

<div class="x_content">
    <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th class="column-title">#</th>
                    <th class="column-title">Thông tin</th>
                    <th class="column-title">Trạng thái</th>
                    <th class="column-title">Có thể thấy</th>
                    <th class="column-title">Kiểu hiển thị</th>
                    <th class="column-title">Tạo mới</th>
                    <th class="column-title">Chỉnh sửa</th>
                    <th class="column-title">Hành động</th>
                </tr>
            </thead>
            <tbody>
                @if (count($items) > 0)
                    @php
                    $index = 0;
                    @endphp
                    @foreach ($items as $item)
                        @php
                        $index++;
                        $id = $item['id'];
                        $name = Highlight::show($item['name'], 'name', $params['search']);
                        $status = Template::showItemStatus($controllerName, $item['id'], $item['status']);
                        $visible = Template::showItemVisible($controllerName, $item['id'], $item['is_home']);
                        $display = Template::showItemSelect($controllerName, $item['id'], $item['display'], 'display');
                        $createdHistory = Template::showItemHistory($item['created_by'], $item['created'],
                        $params['search']);
                        $modifiedHistory = Template::showItemHistory($item['modified_by'], $item['modified'],
                        $params['search']);
                        $optionsBtn = Template::showItemOptions($controllerName, $id);
                        @endphp

                        <tr class="even pointer">
                            <td class="">{{ $id }}</td>
                            <td width="20%">
                                <p><strong>Tiêu đề:</strong> {!! $name !!}</p>
                            </td>
                            <td>{!! $status !!}</td>
                            <td>{!! $visible !!}</td>
                            <td>{!! $display !!}</td>
                            <td>{!! $createdHistory !!}</td>
                            <td>{!! $modifiedHistory !!}</td>
                            <td class="last">{!! $optionsBtn !!}</td>
                        </tr>
                    @endforeach
                @else
                    @include('back-end.templates.empty', ['colspan' => 6])
                @endif
            </tbody>
        </table>
    </div>
</div>

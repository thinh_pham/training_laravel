@php
use App\Helpers\Template as Template;
use App\Helpers\Highlight as Highlight;
@endphp

<div class="x_content">
    <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th class="column-title">#</th>
                    <th class="column-title">Thông tin</th>
                    <th class="column-title">Trạng thái</th>
                    <th class="column-title">Tạo mới</th>
                    <th class="column-title">Chỉnh sửa</th>
                    <th class="column-title">Hành động</th>
                </tr>
            </thead>
            <tbody>
                @if (count($items) > 0)
                    @foreach ($items as $item)
                        @php
                        $index = $item['id'];
                        $name = Highlight::show($item['name'], 'name', $params['search']);
                        $description = Highlight::show($item['description'], 'description', $params['search']);
                        $link = Highlight::show($item['link'], 'link', $params['search']);
                        $thumb = Template::showItemImage($controllerName, $item['thumb'], $item['name']);
                        $status = Template::showItemStatus($controllerName, $index, $item['status']);
                        $createdHistory = Template::showItemHistory($item['created_by'], $item['created'], $params['search']);
                        $modifiedHistory = Template::showItemHistory($item['modified_by'], $item['modified'], $params['search']);
                        $optionsBtn = Template::showItemOptions($controllerName, $index);
                        @endphp

                        <tr class="even pointer">
                            <td class="">{{ $index }}</td>
                            <td width="40%">
                                <p><strong>Tiêu đề:</strong> {!! $name !!}</p>
                                <p><strong>Mô tả:</strong> {!! $description !!}</p>
                                <p><strong>Liên kết:</strong> {!! $link !!}</p>
                                <p>{!! $thumb !!}</p>
                            </td>
                            <td>{!! $status !!}</td>
                            <td>{!! $createdHistory !!}</td>
                            <td>{!! $modifiedHistory !!}</td>
                            <td class="last">{!! $optionsBtn !!}</td>
                        </tr>
                    @endforeach
                @else
                    @include('back-end.templates.empty', ['colspan' => 6])
                @endif
            </tbody>
        </table>
    </div>
</div>

@php
use App\Helpers\Template as Template;
use App\Helpers\FormTemplate as FormTemplate;

$formLabelClass = config('customconfig.template.form.label.class');
$formControlClass = config('customconfig.template.form.control.class');

$controlHiddenID = Form::hidden('id', isset($item['id']) ? $item['id'] : '');
$controlHiddenAvatar = Form::hidden('current-avatar', isset($item['avatar']) ? $item['avatar'] : '');
$controlHiddenTask = Form::hidden('task', 'add');

$statusTmp = config('customconfig.template.status');
$levelTmp = config('customconfig.template.level');
$currenStatus = isset($item['status']) ? $item['status'] : '';

$elements = [
    [
        'label' => Form::label('username', 'Tên người dùng', ['class' => $formLabelClass]),
        'input' => Form::text(
            'username',
            $item['username'] ?? '',
            ['class' => $formControlClass]),
    ],
    [
        'label' => Form::label('fullname', 'Họ tên', ['class' => $formLabelClass]),
        'input' => Form::text(
            'fullname',
            $item['fullname'] ?? '',
            ['class' => $formControlClass]),
    ],
    [
        'label' => Form::label('password', 'Mật khẩu', ['class' => $formLabelClass]),
        'input' => Form::password(
            'password',
            ['class' => $formControlClass]),
    ],
    [
        'label' => Form::label('password_confirmation', 'Nhập lại mật khẩu', ['class' => $formLabelClass]),
        'input' => Form::password(
            'password_confirmation',
            ['class' => $formControlClass]),
    ],
    [
        'label' => Form::label('email', 'Email', ['class' => $formLabelClass]),
        'input' => Form::text(
            'email',
            $item['email'] ?? '',
            ['class' => $formControlClass]),
    ],
    [
        'label' => Form::label('status', 'Trạng thái', ['class' => $formLabelClass]),
        'input' => [
            'active'=> [
                'name' => $statusTmp['active']['name'],
                'control' => Form::radio('status', 'active', true, ['id' => 'status-active']),
            ],
            'inactive'=> [
                'name' => $statusTmp['inactive']['name'],
                'control' => Form::radio('status', 'inactive', $currenStatus === 'inactive' ? true : false, ['id' => 'status-inactive']),
            ],
        ],
        'type' => 'option',
    ],
    [
        'label' => Form::label('level', 'Quyền truy cập', ['class' => $formLabelClass]),
        'input' => Form::select(
            'level',
            $levelTmp,
            $item['level'] ?? null,
            ['class' => $formControlClass]),
    ],
    [
        'label' => Form::label('avatar', 'Hình', ['class' => $formLabelClass]),
        'input' => Form::file('avatar', ['class' => $formControlClass]),
        'avatar' => isset($item['avatar']) ? Template::showItemImage($controllerName, $item['avatar'], $item['name']) : '',
        'type' => 'file',
    ],
    [
        'input' => $controlHiddenID . $controlHiddenAvatar . $controlHiddenTask . Form::submit('Lưu', ['class' => 'btn btn-success', 'style' => 'width: 100%;']),
        'type' => 'btn',
    ],
];
@endphp

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            @include('back-end.templates.x_title', ['title' => 'Thêm mới'])
            <div class="x_content">
                <br />
                @include('back-end.templates.error')
                {{-- form --}}
                {{ Form::open([
                        'url' => route($controllerName . '-save'),
                        'method' => 'POST',
                        'accept-charset' => 'UTF-8',
                        'enctype' => 'multipart/form-data',
                        'class' => 'form-horizontal form-label-left',
                        'data-parsley-validate',
                    ]) }}
                    {{-- form rows --}}
                    {!! FormTemplate::show($elements) !!}
                {{ Form::close() }}
                {{-- // --}}
            </div>
        </div>
    </div>
</div>

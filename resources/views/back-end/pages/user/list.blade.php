@php
use App\Helpers\Template as Template;
use App\Helpers\Highlight as Highlight;
@endphp

<div class="x_content">
    <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th class="column-title">#</th>
                    <th class="column-title">Thông tin</th>
                    <th class="column-title">Hình</th>
                    <th class="column-title">Trạng thái</th>
                    <th class="column-title">Chức vụ</th>
                    <th class="column-title">Tạo mới</th>
                    <th class="column-title">Chỉnh sửa</th>
                    <th class="column-title">Hành động</th>
                </tr>
            </thead>
            <tbody>
                @if (count($items) > 0)
                    @php
                    $index = 0;
                    @endphp
                    @foreach ($items as $item)
                        @php
                        $index++;
                        $name = Highlight::show($item['username'], 'username', $params['search']);
                        $fullName = Highlight::show($item['fullname'], 'fullname', $params['search']);
                        $email = Highlight::show($item['email'], 'email', $params['search']);
                        $avatar = Template::showItemImage($controllerName, $item['avatar'], $item['name']);
                        $status = Template::showItemStatus($controllerName, $item['id'], $item['status']);
                        $level = Template::showItemSelect($controllerName, $item['id'], $item['level'], 'level');
                        $createdHistory = Template::showItemHistory($item['created_by'], $item['created'], $params['search']);
                        $modifiedHistory = Template::showItemHistory($item['modified_by'], $item['modified'], $params['search']);
                        $optionsBtn = Template::showItemOptions($controllerName, $item['id']);
                        @endphp

                        <tr class="even pointer">
                            <td class="">{{ $index }}</td>
                            <td width="20%">
                                <p><strong>Tên người dùng:</strong> {!! $name !!}</p>
                                <p><strong>Tên đầy đủ:</strong> {!! $fullName !!}</p>
                                <p><strong>Email:</strong> {!! $email !!}</p>
                            </td>
                            <td width="15%">{!! $avatar !!}</td>
                            <td>{!! $status !!}</td>
                            <td>{!! $level !!}</td>
                            <td>{!! $createdHistory !!}</td>
                            <td>{!! $modifiedHistory !!}</td>
                            <td class="last">{!! $optionsBtn !!}</td>
                        </tr>
                    @endforeach
                @else
                    @include('back-end.templates.empty', ['colspan' => 6])
                @endif
            </tbody>
        </table>
    </div>
</div>

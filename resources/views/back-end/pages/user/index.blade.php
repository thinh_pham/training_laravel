@php
use App\Helpers\Template as Template;

$filterButtons = Template::showFilterButton($controllerName, $itemsByStatus, $params['filter']['status']);
$searchArea = Template::showSearchArea($controllerName, $params['search']);
@endphp

@extends('back-end.main')

@section('page-content')
    @include('back-end.templates.x_page_header', ['pageIndex' => true, 'pageName' => 'bài viết'])
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                @include('back-end.templates.x_title', ['title' => 'Bộ lọc'])
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-5">
                            {!! $filterButtons !!}
                        </div>
                        <div class="col-md-7">
                            {!! $searchArea !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--box-lists-->
    @include('back-end.templates.notify')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                @include('back-end.templates.x_title', ['title' => 'Danh sách'])
                @include('back-end.pages.user.list')
            </div>
        </div>
    </div>
    <!--end-box-lists-->
    @if (count($items) > 0)
        <!--box-pagination-->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    @include('back-end.templates.x_title', ['title' => 'Phân trang'])
                    @include('pagination.pagination')
                </div>
            </div>
        </div>
        <!--end-box-pagination-->
    @endif

@endsection

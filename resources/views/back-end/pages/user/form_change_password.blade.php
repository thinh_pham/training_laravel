@php
use App\Helpers\Template as Template;
use App\Helpers\FormTemplate as FormTemplate;

$formLabelClass = config('customconfig.template.form.label_edit.class');
$formControlClass = config('customconfig.template.form.control_edit.class');

$controlHiddenID = Form::hidden('id', isset($item['id']) ? $item['id'] : '');
$controlHiddenTask = Form::hidden('task', 'change-password');

$statusTmp = config('customconfig.template.status');
$levelTmp = config('customconfig.template.level');
$currenStatus = isset($item['status']) ? $item['status'] : '';

$elements = [
    [
        'label' => Form::label('password', 'Mật khẩu', ['class' => $formLabelClass]),
        'input' => Form::password(
            'password',
            ['class' => $formControlClass]),
    ],
    [
        'label' => Form::label('password_confirmation', 'Nhập lại mật khẩu', ['class' => $formLabelClass]),
        'input' => Form::password(
            'password_confirmation',
            ['class' => $formControlClass]),
    ],
    [
        'input' => $controlHiddenID . $controlHiddenTask . Form::submit('Lưu', ['class' => 'btn btn-success', 'style' => 'width: 100%;']),
        'type' => 'btn',
    ],
];
@endphp

<div class="col-md-6 col-sm-12 col-xs-12">
    <div class="x_panel">
        @include('back-end.templates.x_title', ['title' => 'Thay đổi mật khẩu'])
        <div class="x_content">
            {{-- form --}}
            {{ Form::open([
                    'url' => route($controllerName . '-change-password'),
                    'method' => 'POST',
                    'accept-charset' => 'UTF-8',
                    'enctype' => 'multipart/form-data',
                    'class' => 'form-horizontal form-label-left',
                    'data-parsley-validate',
                ]) }}
                {{-- form rows --}}
                {!! FormTemplate::show($elements) !!}
            {{ Form::close() }}
            {{-- // --}}
        </div>
    </div>
</div>

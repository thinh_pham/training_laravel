@extends('back-end.main')

@section('page-content')
    @include('back-end.templates.x_page_header', ['pageIndex' => false, 'pageName' => ''])
    @if ($item['id'])
        @include('back-end.templates.error')
        <div class="row">
            @include('back-end.pages.user.form_info')
            @include('back-end.pages.user.form_change_password')
            @include('back-end.pages.user.form_change_level')
        </div>
    @else
        @include('back-end.pages.user.form_add')
    @endif
@endsection

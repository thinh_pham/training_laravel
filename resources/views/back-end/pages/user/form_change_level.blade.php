@php
use App\Helpers\Template as Template;
use App\Helpers\FormTemplate as FormTemplate;

$formLabelClass = config('customconfig.template.form.label_edit.class');
$formControlClass = config('customconfig.template.form.control_edit.class');

$controlHiddenID = Form::hidden('id', isset($item['id']) ? $item['id'] : '');
$controlHiddenTask = Form::hidden('task', 'change-level');

$levelTmp = config('customconfig.template.level');

$elements = [
    [
        'label' => Form::label('level', 'Quyền truy cập', ['class' => $formLabelClass]),
        'input' => Form::select(
            'level',
            $levelTmp,
            $item['level'] ?? null,
            ['class' => $formControlClass]),
    ],
    [
        'input' => $controlHiddenID . $controlHiddenTask . Form::submit('Lưu', ['class' => 'btn btn-success', 'style' => 'width: 100%;']),
        'type' => 'btn',
    ],
];
@endphp

<div class="col-md-6 col-sm-12 col-xs-12">
    <div class="x_panel">
        @include('back-end.templates.x_title', ['title' => 'Thay đổi quyền truy cập'])
        <div class="x_content">
            {{-- form --}}
            {{ Form::open([
                    'url' => route($controllerName . '-change-level'),
                    'method' => 'POST',
                    'accept-charset' => 'UTF-8',
                    'class' => 'form-horizontal form-label-left',
                    'data-parsley-validate',
                ]) }}
                {{-- form rows --}}
                {!! FormTemplate::show($elements) !!}
            {{ Form::close() }}
            {{-- // --}}
        </div>
    </div>
</div>

<!DOCTYPE html>
<html lang="en">

<head>
    @include('back-end.elements.head')
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                @include('back-end.elements.sidebar')
            </div>
            <!-- top navigation -->
            @include('back-end.elements.top-nav')
            <!-- /top navigation -->
            <!-- page content -->
            <div class="right_col" role="main">
                @yield('page-content')
            </div>
            <!-- /page content -->
            <!-- footer -->
            @include('back-end.elements.footer')
            <!-- /footer -->
        </div>
    </div>
    @include('back-end.elements.script')
</body>

</html>

@if (session('notify'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>{{ session('notify') }}</strong>
    </div>
@endif

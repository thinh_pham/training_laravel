@php
    if($controllerName !== 'dashboard') {
        $title = 'Danh sách ' . $pageName;
        $btnAction = sprintf(
            '<a href="%s" class="btn btn-success"><i class="fa fa-plus-circle"></i> Thêm mới</a>',
            route($controllerName . '-form')
        );
        if ($pageIndex == false) {
            $title = 'Biểu mẫu';
            $btnAction = sprintf(
                '<a href="%s" class="btn btn-danger"><i class="fa fa-backward"></i> Quay về</a>',
                route($controllerName)
            );
        }
    } else {
        $title = 'Trang chủ';
        $btnAction = '';
    }
@endphp
<div class="page-header zvn-page-header">
    <div class="zvn-page-header-title">
        <h3>{{ $title }}</h3>
    </div>
    <div class="zvn-add-new pull-right">{!! $btnAction !!}</div>
</div>

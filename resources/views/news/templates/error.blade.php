@if ($errors->any())
    <div class="form-group row">
        <div class="alert alert-warning w-100 text-center">
            @foreach ($errors->all() as $message)
                <strong>{!! $message !!}</strong><br/>
            @endforeach
        </div>
    </div>
@endif

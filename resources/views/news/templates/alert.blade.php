@if (session('notify'))
    <div class="form-group row">
        <div class="alert alert-danger w-100 text-center">
            <strong>{{ session('notify') }}</strong>
        </div>
    </div>
@endif

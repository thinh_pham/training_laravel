@php use App\Helpers\URL as URL; @endphp

<div class="home">
    <div class="parallax_background parallax-window" data-parallax="scroll"
        data-image-src="{{ asset('images/footer.jpg') }} " data-speed="0.8"></div>
    <div class="home_content_container">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="home_content">
                        <div class="home_title">
                        </div>
                        <div class="breadcrumbs">
                            <ul class="d-flex flex-row align-items-start justify-content-start">
                                <li><a href="{{ route('home') }}">Trang chủ</a></li>
                                @if (isset($item['category_id']) && $item['category_id'])
                                    <li>
                                        <a href="{{ URL::linkCategory($item['category_id'], $item['category_name']) }}">
                                            {{ $item['category_name'] }}
                                        </a>
                                    </li>
                                    <li>
                                        <a
                                            href="{{ URL::linkArticle($item['id'], $item['name']) }}">{{ $item['name'] }}</a>
                                    </li>
                                @else
                                    <li><a
                                            href="{{ URL::linkCategory($item['id'], $item['name']) }}">{{ $item['name'] }}</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

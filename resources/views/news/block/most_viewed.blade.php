<div class="most_viewed">
    <div class="sidebar_title">Xem nhiều nhẩt</div>
    <div class="most_viewed_items">
        <!-- Most Viewed Item -->
        <div class="most_viewed_item d-flex flex-row align-items-start justify-content-start">
            <div>
                <div class="most_viewed_num">01.</div>
            </div>
            <div class="most_viewed_content">
                <div class="post_category_small cat_video"><a href="the-loai/category.html">video</a></div>
                <div class="most_viewed_title"><a href="the-loai/single.html">New tech
                        development</a></div>
                <div class="most_viewed_date"><a href="the-loai/the-thao-1.html#">March
                        12, 2018</a></div>
            </div>
        </div>
        <!-- Most Viewed Item -->
        <div class="most_viewed_item d-flex flex-row align-items-start justify-content-start">
            <div>
                <div class="most_viewed_num">02.</div>
            </div>
            <div class="most_viewed_content">
                <div class="post_category_small cat_world"><a href="the-loai/category.html">world</a></div>
                <div class="most_viewed_title"><a href="the-loai/single.html">Robots are
                        taking over</a></div>
                <div class="most_viewed_date"><a href="the-loai/the-thao-1.html#">March
                        12, 2018</a></div>
            </div>
        </div>
        <!-- Most Viewed Item -->
        <div class="most_viewed_item d-flex flex-row align-items-start justify-content-start">
            <div>
                <div class="most_viewed_num">03.</div>
            </div>
            <div class="most_viewed_content">
                <div class="post_category_small cat_technology"><a href="the-loai/category.html">tech</a></div>
                <div class="most_viewed_title"><a href="the-loai/single.html">10 tips to
                        tech world</a></div>
                <div class="most_viewed_date"><a href="the-loai/the-thao-1.html#">March
                        12, 2018</a></div>
            </div>
        </div>
    </div>
</div>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login | Index</title>
    @include('news.elements.head')
</head>

<style>
    body {
        display: flex;
        justify-content: center;
        align-items: center;
        min-width: 100vw;
        min-height: 100vh;
    }

    .signup-heading {
        text-align: center;
        font-weight: 600;
        color: #363a40;
        font-size: 35px;
        margin-bottom: 35px;
    }

    #basic-addon1 {
        cursor: pointer;
    }

    .form-submit {
        display: block;
        margin-top: 20px;
        width: 100%;
        padding: 10px;
        color: #000;
        text-align: center;
        cursor: pointer;
        border: 0;
        background-color: #ddd;
        font-size: 16px;
        font-weight: 500;
        font-family: "Poppins", sans-serif;
        margin-bottom: 20px;
        outline: none;
    }

</style>

<body>
    @yield('page-content')

    @include('news.elements.script')
</body>

</html>

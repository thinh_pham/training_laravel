@php
    use App\Helpers\Template as Template;
    use App\Helpers\URL as URL;

    $name = $item['name'];

    if ($showCategory) {
        $categoryName = $item['category_name'];
        $linkCategory = URL::linkCategory($item['category_id'], $item['category_name']);
    }
    $author = 'tp754629';
    $created = Template::showDateTime($item['created']);
    $content = $length == 'full' ? $item['content'] : Template::showContent($item['content'], $length);
    $linkArticle = URL::linkArticle($item['id'], $item['name']);
@endphp
<div class="post_content">
    @if ($showCategory)
        <div class="post_category cat_technology ">
            <a href="{{ $linkCategory }}">{{ $categoryName }}</a>
        </div>
    @endif
    <div class="post_title">
        <a href="{{ $linkArticle }}">{{ $name }}</a>
    </div>
    <div class="post_info d-flex flex-row align-items-center justify-content-start">
        <div class="post_author d-flex flex-row align-items-center justify-content-start">
            <div class="post_author_name"><a href="#">{{ $author }}</a></div>
        </div>
        <div class="post_date"><a href="#">{{ $created }}</a></div>
    </div>
    @if ($length != 0)
        <div class="post_text">
            <p>{!! $content !!}</p>
        </div>
    @endif
</div>

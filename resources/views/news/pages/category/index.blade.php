@extends('news.main')

@section('page-content')
    <!-- Content -->
    <div class="section-category">
        @include('news.block.breadcrumb', ['item' => $itemCategory])
        <div class="content_container container_category">
            <div class="featured_title">
                <div class="container">
                    <div class="row">
                        <!-- Main Content -->
                        <div class="col-lg-9">
                            @include('news.pages.category.child-index.category', ['item' => $itemCategory])
                        </div>
                        <!-- Sidebar -->
                        <div class="col-lg-3">
                            <div class="sidebar">
                                <!-- Latest Posts -->
                                @include('news.block.latest_post', ['items' => $itemsLatest])
                                <!-- Advertisement -->
                                @include('news.block.advertisement', [])
                                <!-- Most Viewed -->
                                @include('news.block.most_viewed', [])
                                <!-- Tags -->
                                @include('news.block.tags', [])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Content -->
@endsection

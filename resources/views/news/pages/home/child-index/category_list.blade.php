@php use App\Helpers\URL as URL; @endphp

<div class="technology">
    <div class="section_title_container d-flex flex-row align-items-start justify-content-start">
        <div>
            <div class="section_title">{{ $item['name'] }}</div>
        </div>
        <div class="section_bar"></div>
    </div>
    <div class="technology_content">
        @foreach ($item['article'] as $article)
            <div class="post_item post_h_large">
                <div class="row">
                    <div class="col-lg-5">
                        @include('news.partials.article.image', ['item' => $article])
                    </div>
                    <div class="col-lg-7">
                        @include('news.partials.article.content', ['item' => $article, 'length' => 500, 'showCategory' => true])])
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="home_button mx-auto text-center"><a
                    href="{{ URL::linkCategory($item['id'], $item['name']) }}">Xem thêm</a></div>
        </div>
    </div>
</div>

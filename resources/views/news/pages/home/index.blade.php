@extends('news.main')

@section('page-content')
    <div class="home">
        <!-- Home Slider -->
        <div class="home">
            @include('news.block.slider')
        </div>
        <div class="content_container">
            <div class="container">
                <div class="row">
                    <!-- Main Content -->
                    <div class="col-lg-9">
                        <div class="main_content">
                            <!-- Featured -->
                            @include('news.block.featured', ['items' => $itemsFeatured])
                            <!-- Category -->
                            @include('news.pages.home.child-index.category')
                        </div>
                    </div>
                    <!-- Sidebar -->
                    <div class="col-lg-3">
                        <div class="sidebar">
                            <!-- Latest Posts -->
                            @include('news.block.latest_post', ['items' => $itemsLatest])
                            <!-- Advertisement -->
                            @include('news.block.advertisement', [])
                            <!-- Most Viewed -->
                            @include('news.block.most_viewed', [])
                            <!-- Tags -->
                            @include('news.block.tags', [])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('news.partials.article.image', ['item' => $item])
@include('news.partials.article.content', ['item' => $item, 'length' => 'full', 'showCategory' => true])

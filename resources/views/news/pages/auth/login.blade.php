@extends('news.login')

@section('page-content')

    {{ Form::open([
            'url' => route($controllerName . '-postLogin'),
            'method' => 'POST',
            'accept-charset' => 'UTF-8',
            'id' => 'login-form',
        ]) }}
    <h1 class="signup-heading">Đăng Nhập</h1>
    @include('news.templates.error')
    @include('news.templates.alert')
    <div class="form-group row">
        {{ Form::label('email', 'Email', ['class' => 'col-sm-3 col-form-label']) }}
        <div class="col-sm-9">
            {{ Form::text('email', old('email'), ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="form-group row">
        {{ Form::label('password', 'Password', ['class' => 'col-sm-3 col-form-label']) }}
        <div class="col-sm-9">
            <div class="input-group mb-3">
                {{ Form::password('password', ['class' => 'form-control']) }}
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Hiện</span>
                </div>
            </div>
        </div>
    </div>
    {{ Form::submit('Đăng Nhập', ['class' => 'form-submit']) }}
    {{ Form::close() }}

@endsection

@extends('news.main')

@section('page-content')
    <div class="home">
        <div class="content_container">
            <div class="container">
                <div class="row">
                    <!-- Main Content -->
                    <div class="col-lg-9">
                        <div class="main_content">
                            <h3><strong>Bạn không có quyền truy cập vào chức năng này</strong></h3>
                        </div>
                    </div>
                    <!-- Sidebar -->
                    <div class="col-lg-3">
                        <div class="sidebar">
                            <!-- Latest Posts -->
                            @include('news.block.latest_post', ['items' => $itemsLatest])
                            <!-- Advertisement -->
                            @include('news.block.advertisement', [])
                            <!-- Most Viewed -->
                            @include('news.block.most_viewed', [])
                            <!-- Tags -->
                            @include('news.block.tags', [])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

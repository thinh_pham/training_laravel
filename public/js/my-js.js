const btnSearch = document.querySelector("button#btn-search");
const btnClearSearch = document.querySelector("button#btn-clear");
const btnsDelete = document.querySelectorAll("a.btn-delete");
const btnActiveField = document.querySelector("button.btn-active-field");
const inputSearchField = document.querySelector("input[name=search_field]");
const inputSearchValue = document.querySelector("input[name=search_value]");
const selectFields = document.querySelectorAll("a.select-field");
const selectDisplays = document.querySelectorAll(
    "select[name=display-selected]"
);
const loginForm = document.querySelector("#login-form");
const showPasswordIcon = loginForm && loginForm.querySelector("#basic-addon1");
const inputPassword =
    loginForm && loginForm.querySelector('input[type="password"]');

window.addEventListener("load", function() {
    selectFields.forEach(selectField => {
        selectField.addEventListener("click", e => {
            e.preventDefault();

            let field = selectField.dataset.field;
            let fieldName = selectField.innerHTML;
            btnActiveField.innerHTML =
                fieldName + ' <span class="caret"></span>';
            inputSearchField.value = field;
        });
    });

    if (btnSearch && btnClearSearch) {
        btnSearch.addEventListener("click", () => {
            let searchField = inputSearchField.value;
            let searchValue = inputSearchValue.value;
            let params = ["filter_status"];
            let searchParams = new URLSearchParams(window.location.search);
            let link = "";
            params.forEach(param => {
                if (searchParams.has(param))
                    link += param + "=" + searchParams.get(param) + "&";
            });

            window.location.href =
                "?" +
                link +
                "search_field=" +
                searchField +
                "&search_value=" +
                searchValue;
        });

        btnClearSearch.addEventListener("click", () => {
            let path = window.location.pathname;
            let params = ["filter_status", "page"];
            let searchParams = new URLSearchParams(window.location.search);
            let link = "";
            params.forEach(param => {
                if (searchParams.has(param))
                    link += param + "=" + searchParams.get(param);
            });
            if (link !== "") window.location.href = "?" + link;
            else window.location.href = path;
        });
    }

    if (btnsDelete) {
        btnsDelete.forEach(btnDelete => {
            btnDelete.addEventListener("click", e => {
                if (!confirm("Bạn chắc chắn muốn xóa?")) {
                    e.preventDefault();
                    return false;
                }
            });
        });
    }

    if (selectDisplays) {
        selectDisplays.forEach(selectDisplay => {
            selectDisplay.addEventListener("change", () => {
                let selectedValue = selectDisplay.value;
                let url = selectDisplay.dataset.url;
                window.location.href = url.replace("value_new", selectedValue);
            });
        });
    }

    if (showPasswordIcon) {
        showPasswordIcon.addEventListener("click", function() {
            const inputPasswordType = inputPassword.getAttribute("type");
            const labelShowPassword = showPasswordIcon.innerText;
            if (inputPasswordType == "password") {
                inputPassword.setAttribute("type", "text");
                labelShowPassword.innerText = "Ẩn";
            } else {
                inputPassword.setAttribute("type", "password");
                labelShowPassword.innerText = "Hiện";
            }
        });
    }
});

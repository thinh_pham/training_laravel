<?php

use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\News\ArticleController as NewsArticleController;
use App\Http\Controllers\News\AuthController as AuthController;
use App\Http\Controllers\News\CategoryController as NewsCategoryController;
use App\Http\Controllers\News\HomeController;
use App\Http\Controllers\News\NotifyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/**
 * Route: Admin
 */

$prefixAdmin = config('customconfig.url.prefix_admin.admin');

Route::group(['prefix' => $prefixAdmin, 'namespace' => 'Admin', 'middleware' => ['permission.admin']], function () {
    /**
     * Route: Dashboard
     */
    $prefixAdminDashboard = config('customconfig.url.prefix_admin.dashboard');
    Route::group(['prefix' => $prefixAdminDashboard], function () {
        $controllerName = 'dashboard';

        Route::get(
            '/',
            [DashboardController::class, 'index']
        )->name($controllerName);
    });

    /**
     * Route: Slider
     */
    $prefixAdminSlider = config('customconfig.url.prefix_admin.slider');
    Route::group(['prefix' => $prefixAdminSlider], function () {
        $controllerName = 'slider';

        Route::get(
            '/',
            [SliderController::class, 'index']
        )->name($controllerName);

        Route::get(
            '/form/{id?}',
            [SliderController::class, 'form']
        )->where('id', '^[0-9]+$')->name($controllerName . '-form');

        Route::post(
            '/save',
            [SliderController::class, 'save']
        )->name($controllerName . '-save');

        Route::get(
            '/delete/{id}',
            [SliderController::class, 'delete']
        )->where('id', '^[0-9]+$')->name($controllerName . '-delete');

        Route::get(
            '/change-status-{status}/{id}',
            [SliderController::class, 'status']
        )->where('id', '^[0-9]+$')->name($controllerName . '-status');
    });

    /**
     * Route: Category
     */
    $prefixAdminCategory = config('customconfig.url.prefix_admin.category');
    Route::group(['prefix' => $prefixAdminCategory], function () {
        $controllerName = 'category';

        Route::get(
            '/',
            [CategoryController::class, 'index']
        )->name($controllerName);
        Route::get(
            '/form/{id?}',
            [CategoryController::class, 'form']
        )->where('id', '^[0-9]+$')->name($controllerName . '-form');
        Route::post(
            '/save',
            [CategoryController::class, 'save']
        )->name($controllerName . '-save');
        Route::get(
            '/delete/{id}',
            [CategoryController::class, 'delete']
        )->where('id', '^[0-9]+$')->name($controllerName . '-delete');
        Route::get(
            '/change-status-{status}/{id}',
            [CategoryController::class, 'status']
        )->where('id', '^[0-9]+$')->name($controllerName . '-status');
        Route::get(
            '/change-visible-{visible}/{id}',
            [CategoryController::class, 'visible']
        )->where('id', '^[0-9]+$')->name($controllerName . '-visible');
        Route::get(
            '/change-display-{display?}/{id?}',
            [CategoryController::class, 'display']
        )->where('id', '^[0-9]+$')->name($controllerName . '-display');
    });

    /**
     * Route: Article
     */
    $prefixAdminArticle = config('customconfig.url.prefix_admin.article');
    Route::group(['prefix' => $prefixAdminArticle], function () {
        $controllerName = 'article';
        Route::get(
            '/',
            [ArticleController::class, 'index']
        )->name($controllerName);
        Route::get(
            '/form/{id?}',
            [ArticleController::class, 'form']
        )->where('id', '^[0-9]+$')->name($controllerName . '-form');
        Route::post(
            '/save',
            [ArticleController::class, 'save']
        )->name($controllerName . '-save');
        Route::get(
            '/delete/{id}',
            [ArticleController::class, 'delete']
        )->where('id', '^[0-9]+$')->name($controllerName . '-delete');
        Route::get(
            '/change-status-{status}/{id}',
            [ArticleController::class, 'status']
        )->where('id', '^[0-9]+$')->name($controllerName . '-status');
        Route::get(
            '/change-type-{type?}/{id?}',
            [ArticleController::class, 'type']
        )->where('id', '^[0-9]+$')->name($controllerName . '-type');

    });

    /**
     * Route: User
     */
    $prefixAdminUser = config('customconfig.url.prefix_admin.user');
    Route::group(['prefix' => $prefixAdminUser], function () {
        $controllerName = 'user';
        Route::get(
            '/',
            [UserController::class, 'index']
        )->name($controllerName);
        Route::get(
            '/form/{id?}',
            [UserController::class, 'form']
        )->where('id', '^[0-9]+$')->name($controllerName . '-form');
        Route::post(
            '/save',
            [UserController::class, 'save']
        )->name($controllerName . '-save');
        Route::post(
            '/change-password',
            [UserController::class, 'changePassword']
        )->name($controllerName . '-change-password');
        Route::post(
            '/change-level',
            [UserController::class, 'changeLevel']
        )->name($controllerName . '-change-level');
        Route::get(
            '/delete/{id}',
            [UserController::class, 'delete']
        )->where('id', '^[0-9]+$')->name($controllerName . '-delete');
        Route::get(
            '/change-status-{status}/{id}',
            [UserController::class, 'status']
        )->where('id', '^[0-9]+$')->name($controllerName . '-status');
        Route::get(
            '/change-level-{level?}/{id?}',
            [UserController::class, 'level']
        )->where('id', '^[0-9]+$')->name($controllerName . '-level');

    });

});

Route::group(['prefix' => ''], function () {
    $prefix = 'trang-chu';
    Route::group(['prefix' => $prefix], function () {
        Route::get('/', [HomeController::class, 'index'])->name('home');
    });;

    $prefix = 'the-loai';
    Route::group(['prefix' => $prefix], function () {
        Route::get('/{category_name}-{category_id}.html', [NewsCategoryController::class, 'index'])
            ->where('category_name', '[0-9a-zA-z_-]+')
            ->where('category_id', '[0-9]+')
            ->name('category-index');
    });

    $prefix = 'bai-viet';
    Route::group(['prefix' => $prefix], function () {
        Route::get('/{article_name}-{article_id}.html', [NewsArticleController::class, 'index'])
            ->where('article_name', '[0-9a-zA-z_-]+')
            ->where('article_id', '[0-9]+')
            ->name('article-index');
    });

    $prefix = 'auth';
    $controllerName = 'auth';
    Route::group(['prefix' => $prefix], function () use ($controllerName) {
        Route::get('/login', [AuthController::class, 'login'])->name($controllerName . '-login')->middleware('check.login');
        Route::post('/postLogin', [AuthController::class, 'postLogin'])->name($controllerName . '-postLogin');
        Route::get('/logout', [AuthController::class, 'logout'])->name($controllerName . '-logout');
    });

    $prefix = '';
    $controllerName = 'notify';
    Route::group(['prefix' => $prefix], function () use ($controllerName) {
        Route::get('/no-permission', [NotifyController::class, 'noPermission'])
            ->name($controllerName . '-noPermission');
    });

});
